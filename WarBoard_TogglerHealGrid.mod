<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="WarBoard_TogglerHealGrid" version="0.2" date="9/14/2009" >
		<Author name="Computerpunk" email="teromario@yahoo.com" />
		<Description text="HealGrid Toggler module created with LibWBToggler." />
		<VersionSettings gameVersion="1.3.3" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies>
			<Dependency name="LibWBToggler" />
			<Dependency name="HealGrid" forceEnable="false" />
		</Dependencies>
		<Files>
			<File name="WarBoard_TogglerHealGrid.lua" />
		</Files>
		<OnInitialize>
			<CallFunction name="WarBoard_TogglerHealGrid.Initialize" />
		</OnInitialize>
		<WARInfo>
	<Categories>
		<Category name="CAREER_SPECIFIC" />
		<Category name="OTHER" />
	</Categories>
	<Careers>
		<Career name="DISCIPLE" />
		<Career name="RUNE_PRIEST" />
		<Career name="SHAMAN" />
		<Career name="WARRIOR_PRIEST" />
		<Career name="ZEALOT" />
		<Career name="ARCHMAGE" />
	</Careers>
</WARInfo>
	</UiMod>
</ModuleFile>